FROM openjdk:8-alpine
MAINTAINER Alexander Weleczka <mail@aweleczka.de>

ENV BITBUCKET_HOME     /var/atlassian/bitbucket
ENV BITBUCKET_INSTALL  /opt/atlassian/bitbucket
ENV BITBUCKET_VERSION  4.9.1

WORKDIR /
COPY --chown=root:root ./docker-entrypoint.sh /
RUN set -x \
    && chmod +x /docker-entrypoint.sh \
    && apk add --no-cache curl xmlstarlet bash ttf-dejavu libc6-compat git openssh perl \
    && mkdir -p               "${BITBUCKET_HOME}/lib" \
    && chmod -R 700           "${BITBUCKET_HOME}" \
    && mkdir -p               "${BITBUCKET_INSTALL}" \
    && curl -Ls               "https://www.atlassian.com/software/stash/downloads/binary/atlassian-bitbucket-${BITBUCKET_VERSION}.tar.gz" | tar -zx --directory  "${BITBUCKET_INSTALL}" --strip-components=1 --no-same-owner \
    && chmod -R 700           "${BITBUCKET_INSTALL}/conf" \
    && chmod -R 700           "${BITBUCKET_INSTALL}/logs" \
    && chmod -R 700           "${BITBUCKET_INSTALL}/temp" \
    && chmod -R 700           "${BITBUCKET_INSTALL}/work" \
    && xmlstarlet             ed --inplace \
        --delete              "Server/Service/Engine/Host/@xmlValidation" \
        --delete              "Server/Service/Engine/Host/@xmlNamespaceAware" \
                              "${BITBUCKET_INSTALL}/conf/server.xml" \
    && touch -d "@0"          "${BITBUCKET_INSTALL}/conf/server.xml"
VOLUME ["/var/atlassian/bitbucket","/opt/atlassian/bitbucket/logs"]

WORKDIR /var/atlassian/bitbucket
EXPOSE 7990 7999
ENTRYPOINT ["/docker-entrypoint.sh"]

CMD ["/opt/atlassian/bitbucket/bin/catalina.sh", "run"]